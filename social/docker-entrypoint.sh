#!/bin/sh

python manage.py migrate

echo "Running Tests"

python -m pytest --ds=social.settings --cov=social.core

python3 manage.py runserver 0.0.0.0:8000