from rest_framework import viewsets, status
from rest_framework.decorators import detail_route
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from . import models
from .serializers import PostSerializer, UserSerializer, AuthorSerializer


class CreateUserView(CreateAPIView):
    permission_classes = (AllowAny,)
    model = models.User
    serializer_class = UserSerializer


class PostViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication,)
    serializer_class = PostSerializer
    queryset = models.Post.objects.all()

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    @detail_route(methods=['post'])
    def like(self, request, pk=None):
        if models.Post.objects.filter(id=pk).exists():
            post = models.Post.objects.get(id=pk)
            post.like(request.user)
            serializer = self.get_serializer(post)
            return Response(
                serializer.data, status=status.HTTP_200_OK
            )
        return Response(status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['post'])
    def unlike(self, request, pk=None):
        if models.Post.objects.filter(id=pk).exists():
            post = models.Post.objects.get(id=pk)
            post.unlike(request.user)
            serializer = self.get_serializer(post)
            return Response(
                serializer.data, status=status.HTTP_200_OK
            )
        return Response(status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['get'])
    def likes(self, request, pk=None):
        if models.Post.objects.filter(id=pk).exists():
            post = models.Post.objects.get(id=pk)
            return Response(
                {'likes': post.likes_count}, status=status.HTTP_200_OK
            )
        return Response(status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['get'])
    def liked(self, request, pk=None):
        if models.Post.objects.filter(id=pk).exists():
            post = models.Post.objects.get(id=pk)
            return Response(
                {
                    'liked': post.is_liked(request.user)
                },
                status=status.HTTP_200_OK
            )
        return Response(status=status.HTTP_400_BAD_REQUEST)


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication,)
    serializer_class = AuthorSerializer
    queryset = models.User.objects.all()
