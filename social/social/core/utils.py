import importlib

from django.conf import settings


def get_service(fake_service_pass, service_class_setting,
                service_enable_setting):
    is_setting_enabled = getattr(
        settings, service_enable_setting, True
    )

    class_path = fake_service_pass
    if is_setting_enabled:
        class_path = getattr(
            settings, service_class_setting, class_path
        )

    module_name, class_name = class_path.rsplit('.', 1)
    service_class = getattr(
        importlib.import_module(module_name), class_name
    )
    return service_class()


def get_email_verification_service():
    return get_service(
        'social.core.services.FakeEmailVerificationService',
        'EMAIL_VERIFICATION_CLASS',
        'EMAIL_VERIFICATION_ENABLED'
    )


def get_email_data_service():
    return get_service(
        'social.core.services.FakeEmailDataService',
        'EMAIL_INFO_GETTER_CLASS',
        'EMAIL_INFO_DATA_ENABLED'
    )
