import django.contrib.auth.password_validation as validators
from django.core import exceptions
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework_jwt.settings import api_settings
from . import models


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    confirm_password = serializers.CharField(write_only=True)
    token = serializers.SerializerMethodField(read_only=True)
    first_name = serializers.CharField(read_only=True)
    last_name = serializers.CharField(read_only=True)

    class Meta:
        model = models.User
        fields = (
            'email', 'first_name', 'last_name', 'password', 'confirm_password',
            'token', 'id'
        )

    def validate_email(self, value):
        """
        Email verification
        """
        if not models.User.verify_email(value):
            raise serializers.ValidationError(_("Email is not verified"))
        return value

    def validate(self, data):
        """
        Check password confirmation
        """
        password = data.get('password')
        confirm_password = data.pop('confirm_password')
        try:
            user = models.User(**data)
            validators.validate_password(
                password=password, user=user
            )
        except exceptions.ValidationError as e:
            raise serializers.ValidationError(e.messages)

        if password != confirm_password:
            raise serializers.ValidationError(_("Passwords do not match"))
        return super().validate(data)

    def create(self, validated_data):
        user = models.User.objects.create_user(
            email=validated_data['email'],
            password=validated_data['password']
        )
        user.get_additional_info_from_email()

        return models.User.objects.get(email=validated_data['email'])

    def get_token(self, obj):
        """
        Get jwt token for user object
        """
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        payload = jwt_payload_handler(obj)
        token = jwt_encode_handler(payload)
        return token


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.User
        fields = (
            'id', 'email', 'first_name', 'last_name', 'avatar', 'position'
        )


class PostSerializer(serializers.ModelSerializer):
    is_liked = serializers.SerializerMethodField(read_only=True)
    author = AuthorSerializer(read_only=True)

    class Meta:
        model = models.Post
        fields = (
            'id', 'title', 'text', 'created_at',
            'likes_count', 'is_liked', 'author'
        )

    def get_is_liked(self, obj):
        if self.context['request'] and \
                self.context['request'].user.is_authenticated:
            return obj.is_liked(self.context['request'].user)
        return False
