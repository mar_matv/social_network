import json
import logging
from abc import ABCMeta, abstractmethod
from django.conf import settings

import clearbit
from pyhunter import PyHunter

logger = logging.getLogger(__name__)


class EmailVerificationService(metaclass=ABCMeta):
    @abstractmethod
    def verify(self, email):
        pass


class FakeEmailVerificationService(EmailVerificationService):
    def verify(self, email):
        return True


class PyHunterEmailVerificationService(EmailVerificationService):

    def verify(self, email):
        hunter = PyHunter(settings.HUNTER_API_KEY)
        try:
            verification_result = hunter.email_verifier(email)
        except Exception as e:
            logger.error(e)
            return False
        else:
            return 'result' in verification_result and \
                   verification_result['result'] == 'deliverable'


class BaseEmailDataService(metaclass=ABCMeta):
    @abstractmethod
    def extract_additional_info(self, email):
        pass


class FakeEmailDataService(BaseEmailDataService):
    def extract_additional_info(self, email):
        pass


class ClearbitEmailDataService(BaseEmailDataService):
    def extract_additional_info(self, email):
        new_info = None
        clearbit.key = settings.CLEARBEAT_KEY
        lookup = clearbit.Enrichment.find(email=email, stream=True)

        if lookup is not None:
            person_data = lookup['person']
            if person_data:
                new_info = {
                    'first_name': person_data["name"]["givenName"],
                    'last_name': person_data["name"]["familyName"],
                    'position': person_data["employment"]["title"],
                    'avatar': person_data["avatar"]
                }
            return new_info
        else:
            logger.info('Email not found: {}'.format(email))
        return new_info
