from django.conf.urls import url, include
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token

from .views import PostViewSet, CreateUserView, UserViewSet

router = routers.DefaultRouter()
router.register(r'posts', PostViewSet)
router.register(r'users', UserViewSet)

urlpatterns = [
    url(r'^register/', CreateUserView.as_view()),
    url(r'^login/', obtain_jwt_token),
    url(r'^', include(router.urls)),
]
