import pytest
from django.conf import settings

from social.core.utils import get_email_verification_service,\
    get_email_data_service, get_service

from social.core.services import FakeEmailVerificationService,\
    FakeEmailDataService, PyHunterEmailVerificationService, \
    ClearbitEmailDataService


@pytest.mark.parametrize("test_input,expected", [
    (False, FakeEmailVerificationService),
    (True, PyHunterEmailVerificationService)
])
def test_get_email_verification_service(test_input, expected):
    settings.EMAIL_VERIFICATION_ENABLED = test_input
    settings.EMAIL_VERIFICATION_CLASS = \
        'social.core.services.PyHunterEmailVerificationService'
    assert isinstance(
        get_email_verification_service(), expected
    )


@pytest.mark.parametrize("test_input,expected", [
    (False, FakeEmailDataService),
    (True, ClearbitEmailDataService)
])
def test_get_email_data_service(test_input, expected):
    settings.EMAIL_INFO_DATA_ENABLED = test_input
    settings.EMAIL_INFO_GETTER_CLASS = \
        'social.core.services.ClearbitEmailDataService'
    assert isinstance(get_email_data_service(), expected)


@pytest.mark.parametrize("fake_path,service_setting,enable_setting,"
                         "service_class,enable,expected", [
    ('social.core.services.FakeEmailVerificationService',
     'EMAIL_VERIFICATION_CLASS', 'EMAIL_VERIFICATION_ENABLED',
     'social.core.services.PyHunterEmailVerificationService', False,
     FakeEmailVerificationService),

    ('social.core.services.FakeEmailVerificationService',
     'EMAIL_VERIFICATION_CLASS', 'EMAIL_VERIFICATION_ENABLED',
     'social.core.services.PyHunterEmailVerificationService', True,
     PyHunterEmailVerificationService),

    ('social.core.services.FakeEmailDataService',
     'EMAIL_INFO_GETTER_CLASS', 'EMAIL_INFO_DATA_ENABLED',
     'social.core.services.ClearbitEmailDataService', False,
     FakeEmailDataService),

    ('social.core.services.FakeEmailDataService',
     'EMAIL_INFO_GETTER_CLASS', 'EMAIL_INFO_DATA_ENABLED',
     'social.core.services.ClearbitEmailDataService', True,
     ClearbitEmailDataService),
])
def test_get_service(fake_path, service_setting, enable_setting, service_class,
                     enable, expected):
    setattr(settings, service_setting, service_class)
    setattr(settings, enable_setting, enable)
    assert isinstance(
        get_service(fake_path, service_setting, enable_setting), expected
    )
