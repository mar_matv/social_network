import pytest
from social.core.models import User, Post, Like


@pytest.fixture
def author(db, faker):
    return User.objects.create_user(
        email=faker.email(),
        password=faker.password()
    )


@pytest.fixture
def user(db, faker):
    return User.objects.create_user(
        email='m.matviychuk@light-it.net',
        password=faker.password()
    )


@pytest.fixture
def post(db, author, faker):
    return Post.objects.create(
        author=author,
        title=faker.text(64),
        text=faker.text(128)
    )


@pytest.fixture()
def like(db, user, post):
    return Like.objects.create(
        post=post,
        user=user
    )
