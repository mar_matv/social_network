import mock
import pytest
from social.core.services import FakeEmailVerificationService, FakeEmailDataService
from social.core.models import User


@pytest.mark.django_db
class TestUser:

    @mock.patch(
        'social.core.models.get_email_verification_service',
        return_value=FakeEmailVerificationService()
    )
    def test_verify_email_success(self, mock_service):
        assert User.verify_email("test@test.te")

    @mock.patch(
        'social.core.models.get_email_verification_service',
        return_value=FakeEmailVerificationService()
    )
    @mock.patch(
        'social.core.services.FakeEmailVerificationService.verify',
        return_value=False
    )
    def test_verify_email_unsuccess(self, mock_verify, mock_service):
        assert not User.verify_email("test@test.te")

    @mock.patch(
        'social.core.models.get_email_data_service',
        return_value=FakeEmailDataService()
    )
    @mock.patch(
        'social.core.services.FakeEmailDataService.extract_additional_info',
        return_value={'first_name': 'first', 'last_name': 'last'}
    )
    def test_get_additional_info_from_email_success(
            self, mock_data, mock_service, user
    ):
        user.get_additional_info_from_email()
        user = User.objects.get(email=user.email)
        assert user.first_name == 'first'
        assert user.last_name == 'last'

    def test_str(self, user):
        assert str(user) == user.email


@pytest.mark.django_db
class TestPost:

    def test_like(self, post, user):
        assert post.likes_count == 0
        assert not post.is_liked(user)
        post.like(user)
        assert post.likes_count == 1
        assert post.is_liked(user)

    def test_unlike(self, post, user, like):
        assert post.likes_count == 1
        assert post.is_liked(user)
        post.unlike(user)
        assert post.likes_count == 0
        assert not post.is_liked(user)

    def test_is_liked_success(self, post, user, like):
        assert post.is_liked(user)

    def test_is_liked_unsuccess(self, post, user):
        assert not post.is_liked(user)

    def test_likes_count_0(self, post):
        assert post.likes_count == 0

    def test_likes_count_after_like(self, post, like):
        assert post.likes_count == 1

    def test_str(self, post):
        assert str(post) == post.title
