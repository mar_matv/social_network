from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _
from .managers import UserManager
from .utils import get_email_verification_service, get_email_data_service


class User(AbstractUser):
    email = models.EmailField(
        _('email address'),
        unique=True
    )
    avatar = models.CharField(
        max_length=256,
        verbose_name=_('Avatar link'),
        null=True,
        blank=True
    )
    position = models.CharField(
        max_length=128,
        verbose_name=_('Position'),
        null=True,
        blank=True
    )

    username = None
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self):
        return self.email

    @staticmethod
    def verify_email(email):
        verification_service = get_email_verification_service()
        return verification_service.verify(email)

    def get_additional_info_from_email(self):
        """
        Method is used for getting all necessary user info from his email if
        setting EMAIL_INFO_DATA_ENABLED set to True. Application was made
        such way that user info (first_name, last_name, position, avatar)
        are got only by user's email. It could be easily changed to get such
        data by celery task and save it additionally to the data entered
        by user.
        """
        data_service = get_email_data_service()
        new_data = data_service.extract_additional_info(self.email)
        if new_data:
            User.objects.filter(email=self.email).update(**new_data)


class Post(models.Model):
    author = models.ForeignKey(
        User,
        verbose_name=_('Author'),
        related_name="own_posts"
    )
    title = models.CharField(
        verbose_name=_('Title'),
        max_length=128,
    )
    text = models.TextField(
        verbose_name=_('Text'),
        max_length=256
    )
    created_at = models.DateTimeField(
        verbose_name=_('Created at'),
        auto_now_add=True
    )

    def __str__(self):
        return self.title

    def like(self, user):
        if self.author != user:
            self.likes.get_or_create(post=self, user=user)

    def unlike(self, user):
        if self.author != user:
            self.likes.filter(user=user).delete()

    def is_liked(self, user):
        return self.likes.filter(user=user).exists()

    @property
    def likes_count(self):
        return self.likes.count()


class Like(models.Model):
    post = models.ForeignKey(
        Post,
        verbose_name=_('Post'),
        related_name="likes"
    )

    user = models.ForeignKey(
        User,
        verbose_name=_('User'),
        related_name="likes"
    )
    created_at = models.DateTimeField(
        verbose_name=_('Created at'),
        auto_now_add=True
    )

    class Meta:
        unique_together = ('post', 'user')

    def __str__(self):
        return "{} by {}".format(self.post, self.user)
