from django.contrib.admin import site
from .models import Post, Like, User

site.register(Post)
site.register(Like)
site.register(User)
