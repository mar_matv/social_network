# SOCIAL NETWORK API and BOT

This is simple social network api and bot that registers users, create posts and likes them.

## To setup project:

#### From root project directory run
* `cp social/.env_defaults social/.env`
* `vim social/.env`
* `cp bot/.env_defaults bot/.env`
* `vim bot/.env`

Data in `.env` files will override data in .env_defaults.

#### Install docker-compose
https://docs.docker.com/compose/install/

#### Run
* `docker-compose up -d`

Social network api will be available by address `http://127.0.0.1:8000/api/`


## To run bot use command:

`docker-compose run bot python bot.py`


## To create superuser for login to admin run

`docker-compose exec web python manage.py createsuperuser`
