import random
import string

from config import NUMBER_OF_USERS, EMAIL_PREFIX, EMAIL_DOMAIL


class User(object):
    email = None
    password = None
    token = None
    likes = 0
    posts = None
    liked_posts = None

    def __init__(self):
        self.email = self._generate_email()
        self.password = self._generate_password()
        self.posts = []
        self.liked_posts = []

    def _generate_email(self):
        number = random.randint(1, NUMBER_OF_USERS * 1000)
        return "{prefix}+{number}@{domain}".format(
            prefix=EMAIL_PREFIX,
            number=number,
            domain=EMAIL_DOMAIL
        )

    def _generate_password(self):
        return ''.join(
            random.choice(
                string.ascii_uppercase + string.digits
            ) for _ in range(10)
        )

    def set_token(self, token):
        self.token = token

    def get_token(self):
        return self.token

    def add_post(self, post):
        self.posts.append(post)

    def get_posts_count(self):
        return len(self.posts)

    def likes_inc(self):
        self.likes += 1

    def get_likes(self):
        return self.likes

    def add_liked_post(self, post_id):
        self.liked_posts.append(post_id)

    def is_post_liked(self, post):
        return post.id in self.liked_posts

    def __lt__(self, other):
        return self.get_posts_count() > other.get_posts_count()


class Post(object):
    id = 0
    likes = 0

    def __init__(self, _id):
        self.id = _id

    def likes_inc(self):
        self.likes += 1

    def get_likes(self):
        return self.likes
