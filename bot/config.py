import os


def env(key, default=None):
    return os.environ.get(key, default)


NUMBER_OF_USERS = int(env('NUMBER_OF_USERS'))
MAX_POSTS_PER_USER = int(env('MAX_POSTS_PER_USER'))
MAX_LIKES_PER_USER = int(env('MAX_LIKES_PER_USER'))

SOCIAL_NETWORK_URL = env('SOCIAL_NETWORK_URL')

EMAIL_DOMAIL = env('EMAIL_DOMAIL')
EMAIL_PREFIX = env('EMAIL_PREFIX')

LOGGER_FORMAT = env('LOGGER_FORMAT')
