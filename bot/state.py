from models import User, Post
import random


class State(object):
    users = []
    current_user = None

    def create_new_user(self):
        self.current_user = User()
        return self.current_user

    def user_registered(self, data):
        self.current_user.set_token(data["token"])
        self.users.append(self.current_user)

    def user_not_reached_likes_limit(self, user, limit):
        return user.get_likes() < limit

    def post_added(self, user, post_id):
        post_id = self.get_post_uid(post_id)
        post = Post(post_id)
        user.add_post(post)
        return post

    def post_liked(self, user, post):
        user.likes_inc()
        user.add_liked_post(post.id)
        post.likes_inc()

    def sort_users(self):
        self.users.sort()

    def get_not_liked_post(self, current_user):
        users_for_liking = self.get_users_with_0_like_post()
        if current_user in users_for_liking:
            users_for_liking.remove(current_user)

        rand_post = None
        all_posts = []
        for item in users_for_liking:
            all_posts.extend(item.posts)

        while len(all_posts) > 0:
            rand_index = random.randrange(0, len(all_posts))
            rand_post = all_posts[rand_index]

            if not current_user.is_post_liked(rand_post):
                break
            else:
                all_posts.remove(rand_post)
        return rand_post

    def get_users_with_0_like_post(self):
        users_with_no_like_posts = []
        for item in self.users:
            for post in item.posts:
                if post.get_likes() == 0:
                    users_with_no_like_posts.append(item)
                    break
        return users_with_no_like_posts

    def all_posts_liked(self):
        return len(self.get_users_with_0_like_post()) == 0

    def all_posts_liked_by_user(self, user):
        no_liked_list = self.get_users_with_0_like_post()
        return len(no_liked_list) == 1 and user in no_liked_list

    def get_post_uid(self, data):
        return data["id"]
