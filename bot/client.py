import aiohttp


class IClient(object):
    async def post_request(self, url, data, headers):
        raise NotImplementedError


class AIOHTTPClient(IClient):
    _session = None

    def __init__(self):
        self._session = aiohttp.ClientSession()

    def __del__(self):
        self._session.close()

    async def post_request(self, url, data, headers):
        async with self._session.post(url, data=data, headers=headers) as resp:
            data = await resp.json()
            return resp.status, data
