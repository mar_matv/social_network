import json
from faker import Factory
fake = Factory.create()


class NetworkBase(object):
    headers = None
    registration_url = "register"
    posts_url = "posts"

    def __init__(self, _domain, _api_prefix, client):
        self.domain = _domain
        self.api_prefix = _api_prefix
        self.client = client

    def _get_url(self, api_url):
        return "{domain}/{api_prefix}/{api_url}".format(
            domain=self.domain, api_prefix=self.api_prefix, api_url=api_url
        )

    def get_registration_url(self):
        return self._get_url(self.registration_url)

    def get_posts_url(self):
        return self._get_url(self.posts_url)

    def get_like_url(self, post_id):
        raise NotImplementedError

    def get_register_payload(self, user=None):
        raise NotImplementedError

    def get_post_payload(self, post=None):
        raise NotImplementedError

    def get_headers(self):
        return self.headers

    def set_headers(self, headers):
        self.headers = headers

    def update_headers(self, key, value):
        if self.headers:
            self.headers[key] = value
        else:
            self.headers = {key: value}

    async def register_user(self, user):
        url = self.get_registration_url()
        data = json.dumps(self.get_register_payload(user))
        return await self.client.post_request(url, data, self.headers)

    async def create_post(self, user, post=None):
        url = self.get_posts_url()
        data = json.dumps(self.get_post_payload(post))
        return await self.client.post_request(url, data, self.headers)

    async def like_post(self, user, post):
        url = self.get_like_url(post.id)
        return await self.client.post_request(url, None, self.headers)


class JWTSocialNetwork(NetworkBase):
    registration_url = "register/"
    posts_url = "posts/"

    def __init__(self, _domain, _api_prefix, client):
        super().__init__(_domain, _api_prefix, client)
        self.set_headers({'content-type': 'application/json'})

    def get_like_url(self, post_id):
        return "{}{}/like/".format(self.get_posts_url(), post_id)

    def get_register_payload(self, user=None):
        return {
            'email': user.email,
            'password': user.password,
            'confirm_password': user.password
        }

    def get_post_payload(self, post=None):
        return {
            'title': fake.text(64),
            'text': fake.text(250)
        }

    async def create_post(self, user, post=None):
        self.update_headers('Authorization', 'JWT {}'.format(user.get_token()))
        return await super().create_post(user, post)

    async def like_post(self, user, post):
        self.update_headers('Authorization', 'JWT {}'.format(user.get_token()))
        return await super().like_post(user, post)
