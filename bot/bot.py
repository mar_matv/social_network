import asyncio

import logging
import random

from config import *

from client import AIOHTTPClient
from network import JWTSocialNetwork
from state import State

logging.basicConfig(format=LOGGER_FORMAT, datefmt='[%H:%M:%S]')
log = logging.getLogger()
log.setLevel(logging.INFO)


class AllPostsLikesException(Exception):
    pass


class UserAllPostsLikesException(Exception):
    pass


class Behaviour(object):
    social_network = None
    state = State()

    def __init__(self, network_interaction):
        self.social_network = network_interaction

    async def register(self, amount=0):
        counter = 0
        while counter < amount:
            status, resp = await self.social_network.register_user(
                self.state.create_new_user()
            )
            if status == 201:
                self.state.user_registered(resp)
                log.info("Registered user with email: {}".format(resp["email"]))
                counter += 1
            else:
                log.error(
                    "During registration error occurred: {}".format(resp)
                )

    async def proceed_posting(self, max_posts_per_user):
        for user in self.state.users:
            limit = random.randint(1, max_posts_per_user)
            tasks = [self.create_post(user)
                     for i in range(0, limit)]
            await asyncio.gather(*tasks)

    async def create_post(self, user):
        status, resp = await self.social_network.create_post(user)
        if status == 201:
            post = self.state.post_added(user, resp)
            log.info("Post created {} by user {}".format(post.id, user.email))
        else:
            log.error(
                "During post creation error occurred: {}".format(resp))

    async def like_post(self, user, post):
        status, resp = await self.social_network.like_post(user, post)
        if status == 200:
            self.state.post_liked(user, post)
            log.info("Post {} liked by {}".format(post.id, user.email))
        else:
            log.error("During liking error occurred: {}".format(resp))

    async def proceed_liking(self, likes_limit):
        self.state.sort_users()
        for user in self.state.users:
            log.info("User {} with {} posts started liking"
                     .format(user.email, user.get_posts_count()))
            await self.proceed_user_liking(user, likes_limit)

    async def proceed_user_liking(self, user, likes_limit):

        while self.state.user_not_reached_likes_limit(user, likes_limit):

            if self.state.all_posts_liked():
                raise AllPostsLikesException
            elif self.state.all_posts_liked_by_user(user):
                break

            try:
                await self.make_like(user)
            except UserAllPostsLikesException:
                break

    async def make_like(self, user):
        post_for_like = self.state.get_not_liked_post(user)
        if not post_for_like:
            raise UserAllPostsLikesException
        else:
            await self.like_post(user, post_for_like)


class Bot(object):
    behaviour = None

    def __init__(self, network_interaction):
        self.behaviour = Behaviour(network_interaction)

    async def run(self):
        await self.behaviour.register(NUMBER_OF_USERS)
        if MAX_POSTS_PER_USER > 0:
            await self.behaviour.proceed_posting(MAX_POSTS_PER_USER)

            try:
                await self.behaviour.proceed_liking(MAX_LIKES_PER_USER)
            except AllPostsLikesException:
                pass


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    network = JWTSocialNetwork(SOCIAL_NETWORK_URL, "api", AIOHTTPClient())
    bot = Bot(network)
    log.info("BOT's work is started")
    loop.run_until_complete(bot.run())
    log.info("BOT's work is completed")
    loop.close()
